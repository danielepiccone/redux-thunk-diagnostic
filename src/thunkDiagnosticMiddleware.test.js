/* eslint-env jest */

import unexpected from 'unexpected'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import sinon from 'sinon'

import {
  createThunkDiagnostic,
  CALLER_ID,
  CallMap
} from './thunkDiagnosticMiddleware'

const expect = unexpected.clone().use(require('unexpected-sinon'))
const sleep = n => new Promise(resolve => setTimeout(resolve, n))

describe('thunk diagnostic middleware', () => {
  let nextSpy
  let callMap
  let logger
  let store

  function createStoreWithMiddleware(options) {
    callMap = new CallMap()
    logger = { action: () => {}, thunk: () => {} }

    const middleware = createThunkDiagnostic({
      logger,
      callMap
    })

    const nextMiddleware = store => next => {
      nextSpy = sinon.spy(next).named('nextSpy')

      return action => {
        return nextSpy(action)
      }
    }

    return createStore(
      () => {},
      applyMiddleware(middleware(options), thunk, nextMiddleware)
    )
  }

  beforeEach(() => {
    store = createStoreWithMiddleware()
  })

  it('passes down an action', () => {
    store.dispatch({ type: 'bar' })

    return expect(nextSpy, 'to have a call satisfying', [
      { type: 'bar' }
    ])
  })

  it('wraps a thunk so that it dispatches an action with a [CALLER_ID] Symbol', () => {
    const thunk = dispatch => dispatch({ type: 'foo' })
    store.dispatch(thunk)

    return expect(nextSpy, 'to have a call satisfying', [
      { type: 'foo', [CALLER_ID]: expect.it('to be defined') }
    ])
  })

  it('adds the thunk to the callMap when called', () => {
    const thunk = dispatch => dispatch({ type: 'foo' })
    store.dispatch(thunk)

    const [ action ] = nextSpy.firstCall.args

    return expect(callMap.get(action[CALLER_ID]), 'to be defined')
  })

  it('clears the call map as soon the browser allocates a new task', () => {
    const thunk = dispatch => dispatch({ type: 'foo' })
    store.dispatch(thunk)

    return Promise.resolve().then(() => {
      return new Promise(resolve => {
        setTimeout(() => {
          expect(callMap.values().length, 'to equal', 0)
          resolve()
        })
      })
    })
  })

  it('clears the call map after a chain of dispatches has been completed', () => {
    const innerInnerThunk = dispatch => dispatch({ type: 'foo' })
    const innerThunk = dispatch => dispatch(innerInnerThunk)
    const thunk = dispatch => dispatch(innerThunk)

    return Promise.resolve().then(() => {
      return new Promise(resolve => {
        setTimeout(() => {
          expect(callMap.values().length, 'to equal', 0)
          resolve()
        })
      })
    })
  })

  describe('concurrent calls', () => {
    let clock
    let sleep

    beforeEach(() => {
      clock = sinon.useFakeTimers()
    })

    afterEach(() => {
      clock.restore()
    })

    it('clears the call map after a chain of pending call has resolved', () => {
      const innerInnerThunk = dispatch => dispatch({ type: 'foo' })
      const innerThunk = dispatch => {
        return new Promise(resolve => {
          setTimeout(() => dispatch(innerInnerThunk), 200)
          clock.tick(200)
          resolve()
        })
      }
      const thunk = dispatch => dispatch(innerThunk)

      return store.dispatch(thunk).then(() => {
        return expect(callMap.values().length, 'to equal', 3)
      }).then(() => {
        return new Promise(resolve => {
          setTimeout(() => {
            expect(callMap.values().length, 'to equal', 0)
            resolve()
          })
          clock.tick()
        })
      })
    })
  })

  describe('when logging', () => {
    it('logs an action if it is an action', () => {
      logger.action = sinon.spy()

      store.dispatch({ type: 'foo' })

      return expect(logger.action, 'was called')
    })

    it('logs a thunk when it is dispatched', () => {
      logger.thunk = sinon.spy()

      const thunk = dispatch => dispatch({ type: 'foo' })
      store.dispatch(thunk)
      return expect(logger.thunk, 'was called')
    })

    it('logs multiple levels for a nested thunk when it is dispatched', () => {
      logger.thunk = sinon.spy()

      const innerThunk = dispatch => dispatch({ type: 'foo' })
      const thunk = dispatch => {
        dispatch(innerThunk)
      }
      store.dispatch(thunk)
      return expect(logger.thunk, 'was called twice')
    })

    describe('concurrent calls', () => {
      let clock
      let sleep

      beforeEach(() => {
        clock = sinon.useFakeTimers()
      })

      afterEach(() => {
        clock.restore()
      })

      it('logs the maximum level reached also if one of the callers finished', () => {
        logger.thunk = sinon.spy()

        // this action dispatches internally but is pending for 500ms
        const innerInnerThunk = dispatch => {
          return new Promise(resolve => {
            setTimeout(() => {
              dispatch({ type: 'foo' })
            }, 300)
            setTimeout(resolve, 500)
            clock.tick(500)
          })
        }

        const innerThunk = dispatch => dispatch(innerInnerThunk)
        const thunk = dispatch => dispatch(innerThunk)

        return store.dispatch(thunk).then(() => {
          expect(logger.thunk, 'was called times', 3)
          expect(logger.thunk, 'to have calls satisfying', [
            [ 0, expect.it('to be undefined'), expect.it('to be defined') ],
            [ 1, expect.it('to be defined'), expect.it('to be defined') ],
            [ 2, expect.it('to be defined'), expect.it('to be defined') ],
          ])
        })
      })

      it('logs the maximum level reached between concurrent calls', () => {
        logger.thunk = sinon.spy()

        const innerInnerThunk = dispatch => {
          return new Promise(resolve => {
            setTimeout(() => {
              dispatch({ type: 'foo' })
              resolve()
            }, 100)
            clock.tick(100)
          })
        }
        const innerThunk1 = dispatch => dispatch(innerInnerThunk)
        const innerThunk2 = dispatch => {
          return new Promise(resolve => {
            setTimeout(() => {
              dispatch({ type: 'bar' })
              resolve()
            }, 200)
            clock.tick(200)
          })
        }

        const thunk = dispatch => {
          dispatch(innerThunk1)
          dispatch(innerThunk2)
          return Promise.resolve()
        }

        return store.dispatch(thunk).then(() => {
            expect(logger.thunk, 'was called times', 5)
            expect(logger.thunk, 'to have calls satisfying', [
              [ 0, expect.it('to be undefined'), expect.it('to be defined') ],
              [ 1, expect.it('to be defined'), expect.it('to be defined') ],
              [ 2, expect.it('to be defined'), expect.it('to be defined') ],
              [ 0, expect.it('to be undefined'), expect.it('to be defined') ],
              [ 1, expect.it('to be defined'), expect.it('to be defined') ],
            ])
          })
      })
    })

    describe('given maxLevel 2', () => {
      beforeEach(() => {
        store = createStoreWithMiddleware({ maxLevel: 2 })
      })

      it('warns for 3 dispatch calls', () => {
        logger.warnMaxNesting = sinon.spy()

        const innerInnerThunk = dispatch => dispatch({ type: 'foo' })
        const innerThunk = dispatch => dispatch(innerInnerThunk)
        const thunk = dispatch => dispatch(innerThunk)

        store.dispatch(thunk)
        return expect(logger.warnMaxNesting, 'was called')
      })

      it('does not warn for 2 dispatch calls', () => {
        logger.warnMaxNesting = sinon.spy()

        const innerThunk = dispatch => dispatch({ type: 'foo' })
        const thunk = dispatch => dispatch(innerThunk)

        store.dispatch(thunk)
        return expect(logger.warnMaxNesting, 'was not called')
      })
    })

    describe('given silent true', () => {
      beforeEach(() => {
        store = createStoreWithMiddleware({ silent: true })
      })

      it('does not log an action', () => {
        logger.action = sinon.spy()
        store.dispatch({ type: 'foo' })
        return expect(logger.action, 'was called')
      })

      it('does not log a thunk', () => {
        logger.thunk = sinon.spy()
        const thunk = dispatch => dispatch({ type: 'foo' })
        store.dispatch(thunk)
        return expect(logger.thunk, 'was called')
      })
    })
  })
})
