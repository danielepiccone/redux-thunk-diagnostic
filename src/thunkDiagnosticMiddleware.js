const uid = () => Math.floor((1 + Math.random()) * 0x100000000)
  .toString(16)
  .substring(1)

const maybePromise = (promise, callback) => {
  if (promise && typeof promise.then === 'function') {
    return promise.then(callback)
  } else {
    Promise.resolve().then(callback)
    return promise
  }
}

export class Logger {
  getPadding (level) {
    return new Array(level).fill('  ').join('')
  }

  action (level, callerId, action) {
    if (callerId) {
      console.log(
        `${this.getPadding(level)}→ Action from ${callerId} %c${action.type}`,
        'color: red'
      )
    } else {
      console.log(
        `${this.getPadding(level)}→ Action %c${action.type}`,
        'color: red'
      )
    }
  }

  thunk (level, callerId, thunkId) {
    if (callerId) {
      console.log(
        `%c${this.getPadding(level)}↳ Thunk ${thunkId} from ${callerId}`,
        'color: blue'
      )
    } else {
      console.log(
        `%c${this.getPadding(level)}↳ Thunk ${thunkId}`,
        'color: blue'
      )
    }
  }

  warnMaxNesting(level, maxLevel) {
    console.warn(`Maximum allowed thunk nesting ${maxLevel}, current ${level}`)
  }
}

class CallNode {
  constructor({ callerId = null, data = {} }) {
    this.id = uid()
    this.callerId = callerId
    this.data = data
    this.isEnded = false
  }

  markEnded() {
    this.isEnded = true
  }
}

export class CallMap {
  constructor () {
    this.callsById = {}
  }

  set (id, value) {
    this.callsById[id] = value
  }

  get (id) {
    return this.callsById[id]
  }

  delete (id) {
    delete this.callsById[id]
  }

  values () {
    return Object.values(this.callsById);
  }
}

function getCallStackFromCallId (callMap, id) {
  const call = callMap.get(id)
  if (call) {
    if (call.callerId) {
      return [...getCallStackFromCallId(callMap, call.callerId), call]
    } else {
      return [call]
    }
  } else {
    return []
  }
}

let isSweeping = false
function sweepMarkedCalls(callMap) {
  if (isSweeping) {
    return false
  }
  isSweeping = true

  const calls = callMap.values()
  // get all the caller ids
  const registeredCallerIds = calls.map(({ callerId }) => callerId)
  // get the calls that have an id that is not registered as caller
  const leafCandidates = callMap.values()
    .filter(({ id }) => !registeredCallerIds.includes(id))

  // clear each not runnig branch starting from the leaves
  for (let call of leafCandidates) {
    const stack = getCallStackFromCallId(callMap, call.id)

    for (
      let currentCall = stack.pop();
      currentCall && currentCall.isEnded;
      currentCall = stack.pop()
    ) {
        callMap.delete(currentCall.id)
    }
  }

  isSweeping = false
  return true
}

export const CALLER_ID = Symbol('CALLER_ID')

export function createThunkDiagnostic ({ logger, callMap }) {
  const thunkDiagnostic = options => {
    options = options || {}
    const maxLevel = typeof options.maxLevel === 'number' ? options.maxLevel : null
    const silent = !!options.silent || false

    if (silent) {
      logger.action = () => {}
      logger.thunk = () => {}
    }

    let sweepTimeout = null

    return store => next => action => {
      if (typeof action === 'function') {
        const wrappedThunk = (dispatch, getState, extraArgument) => {
          const callerId = action[CALLER_ID]

          const localDispatch = (localAction) => {
            const call = new CallNode({
              callerId,
              data: typeof localAction !== 'function' ? localAction : null
            });

            const thunkId = call.id;
            localAction[CALLER_ID] = thunkId

            callMap.set(thunkId, call);

            const level = getCallStackFromCallId(callMap, callerId).length
            logger.thunk(level, callerId, thunkId)

            if (maxLevel && level + 1 > maxLevel) {
              logger.warnMaxNesting(level + 1, maxLevel)
            }

            return maybePromise(dispatch(localAction), () => {
              call.markEnded()

              // schedule a task to cleanup ended calls
              sweepTimeout = setTimeout(() => {
                sweepMarkedCalls(callMap)
              })
            })
          }

          return action(localDispatch, getState, extraArgument)
        }

        clearTimeout(sweepTimeout)

        return next(wrappedThunk)
      } else {
        const level = getCallStackFromCallId(callMap, action[CALLER_ID]).length
        logger.action(level, action[CALLER_ID], action)
        return next(action)
      }
    }
  }

  return thunkDiagnostic
}

export default createThunkDiagnostic({
  logger: new Logger(),
  callMap: new CallMap()
})
