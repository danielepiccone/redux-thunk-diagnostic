# Redux thunk diagnostic

### Scope

This is a diagnostic middleware that keeps track and visualizes how many times `dispatch()` has been called with a thunk.

### Usage

```
  import thunkDiagnosticMiddleware from 'redux-thunk-diagnostic';

  // ... then when creating the store

  applyMiddleware(thunkDiagnosticMiddleware(options))

```

### Options

You can provide the following options.

#### maxLevel(Number)
Warns if there have been more than n consecutive dispatches within a thunk

```

  thunkDiagnostcMiddleware({ maxLevel: n })

```

#### silent(Boolean)
Do not log, use in conjunction with `maxLevel` to only get the warnings

```

  thunkDiagnostcMiddleware({ silent: true })

```
